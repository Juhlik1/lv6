﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV6
{
    class CareTaker
    {
        public List<Memento> PreviousState { get; set; }
        public CareTaker() {
            PreviousState = new List<Memento>();
        }
        public void AddToCare(Memento memento)
        {
            PreviousState.Add(memento);
        }
        public void RemoveFirstAdded() {
            PreviousState.RemoveAt(0);
        }
        public void RemoveSpecific(Memento memento){
            PreviousState.Remove(memento);
        }
        public Memento GetLastAdded() {
            return PreviousState.Last();
        }
    }
}
