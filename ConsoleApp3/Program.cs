﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;

namespace LV6
{
    class Program
    {
        static void Main(string[] args)
        {
            //zad 1
            //Note firstnote = new Note("Prva biljeska", "Neki tekst");
            //Note secondnote = new Note("Druga biljeska", "Neki drugi tekst");
            //Note thirdnote = new Note("Teca biljeska", "Neki teci tekst");
            //List<Note> NoteList = new List<Note>();
            //NoteList.Add(firstnote);
            //NoteList.Add(secondnote);
            //NoteList.Add(thirdnote);
            //Notebook Firstnotebook = new Notebook(NoteList);
            //IAbstractIterator iterator = Firstnotebook.GetIterator();
            //while (!iterator.IsDone)
            //{
            //    iterator.Current.Show();
            //    iterator.Next();
            //}
            //zad 2
            //Product firstProduct = new Product("Prvi produkt", 5000);
            //Product secoundProduct = new Product("drugi produkt", 1);
            //Product thirdProduct = new Product("treci produkt", 48623);
            //List<Product> ListOfProduct = new List<Product>();
            //ListOfProduct.Add(firstProduct);
            //ListOfProduct.Add(secoundProduct);
            //ListOfProduct.Add(thirdProduct);
            //Box box = new Box(ListOfProduct);
            //IAbstractIteratorBox iterator2 = box.GetIterator();
            //while (!iterator2.IsDone)
            //{
            //    Console.WriteLine(iterator2.Current);
            //    iterator2.Next();
            //}
            //zad3 
            //string dateString = "5/14/2020 23:59:59 ";
            //DateTime date1 = DateTime.Parse(dateString, System.Globalization.CultureInfo.InvariantCulture);
            //ToDoItem toDoItem = new ToDoItem("Naki naslov", "Neki drugi text", date1);
            //ToDoItem toDoItem1 = new ToDoItem("Naki tamo naslov", "Neki teci text", date1);
            //Memento memento1 = toDoItem.StoreState();
            //Memento memento2 = toDoItem1.StoreState();
            //CareTaker careTaker = new CareTaker();
            //careTaker.AddToCare(memento1);
            //careTaker.AddToCare(memento2);
            //Console.WriteLine("Naslov:" + careTaker.GetLastAdded().Title);
            //Console.WriteLine("Vrijeme kreiranja:" + careTaker.GetLastAdded().CreationTime);
            //Console.WriteLine("Tekst:" + careTaker.GetLastAdded().Text);
            //Console.WriteLine("Vrijeme do kad teba izvrsiti:" + careTaker.GetLastAdded().TimeDue);
            //zad4
            //BankAccount bankAccount = new BankAccount("Josip Uhlik", "Crkvena 1", 10);
            //MementoBankAccount memento3 =bankAccount.storeState();
            //Console.WriteLine("Vlasnik:"+memento3.ownerName);
            //Console.WriteLine("Adresa:" + memento3.ownerAddress);
            //Console.WriteLine("Novac na racunu:" + memento3.balance+"Kn");
            //zad5
            //FileLogger fileLogger = new FileLogger(MessageType.INFO, "D:\\text.txt");
            //ConsoleLogger consoleLogger = new ConsoleLogger(MessageType.INFO);
            //FileLogger fileLogger2 = new FileLogger(MessageType.ERROR, "D:\\text.txt");
            //ConsoleLogger consoleLogger2 = new ConsoleLogger(MessageType.ERROR);
            //FileLogger fileLogger3 = new FileLogger(MessageType.WARNING, "D:\\text.txt");
            //ConsoleLogger consoleLogger3 = new ConsoleLogger(MessageType.WARNING);
            //FileLogger fileLogger4 = new FileLogger(MessageType.ALL, "D:\\text.txt");
            //ConsoleLogger consoleLogger4 = new ConsoleLogger(MessageType.ALL);
            //fileLogger.SetNextLogger(consoleLogger);
            //consoleLogger.SetNextLogger(fileLogger2);
            //fileLogger2.SetNextLogger(consoleLogger2);
            //consoleLogger2.SetNextLogger(fileLogger3);
            //fileLogger3.SetNextLogger(consoleLogger4);
            //consoleLogger4.SetNextLogger(fileLogger4);
            //fileLogger.Log("Pozz", MessageType.INFO);
            //fileLogger.Log("Pozz", MessageType.ERROR);
            //fileLogger.Log("Pozz", MessageType.WARNING);
            //fileLogger.Log("Pozz", MessageType.ALL);
        }
    }
}
