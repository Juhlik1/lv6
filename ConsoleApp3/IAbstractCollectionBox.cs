﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6
{
    interface IAbstractCollectionBox
    {
        IAbstractIteratorBox GetIterator();
    }
}
