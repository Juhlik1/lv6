﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LV6
{
    class FileLogger : AbstractLogger

    {
        private string filePath;
        public FileLogger(MessageType messageType, string filePath) : base(messageType)
        {
            this.filePath = filePath;
        }
        protected override void WriteMessage(string message, MessageType type)
        {
            {
                using (StreamWriter streamWriter = new StreamWriter(filePath,true))
                {
                    streamWriter.WriteLine(type + ": " + DateTime.Now);
                    streamWriter.WriteLine(new string('=', message.Length));
                    streamWriter.WriteLine(message);
                    streamWriter.WriteLine(new string('=', message.Length) + "\n");
                    streamWriter.Close();
                }
            }

        } 
    } 
}