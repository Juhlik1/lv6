﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6
{
    class MementoBankAccount
    {
        public string ownerName { get; private set; }
        public string ownerAddress { get; private set; }
        public decimal balance { get; private set; }
        public MementoBankAccount(string ownerName,string ownerAddress,decimal balance){
            this.ownerAddress = ownerAddress;
            this.ownerName = ownerName;
            this.balance = balance;
        }
    }
}
